package ru.t1.vlvov.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.t1.vlvov.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskChangeStatusByIdResponse extends AbstractTaskResponse {

    public TaskChangeStatusByIdResponse(TaskDTO task) {
        super(task);
    }

}
