package ru.t1.vlvov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void save() {
        bootstrap.getDomainService().saveDataBackup();
    }

    public void load() {
        if (Files.exists(Paths.get(DomainService.FILE_BACKUP)))
            bootstrap.getDomainService().loadDataBackup();
    }

    public void stop() {
        es.shutdown();
    }

    public void start() {
        load();
        es.scheduleAtFixedRate(this::save, 0, 3, TimeUnit.SECONDS);
    }

}
